<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css">
    @livewireStyles
    @livewireScripts
    @yield('css')
    <style>
        .loader {
            border: 0;
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            position: absolute;
            top: 0;
            right: 0;
            left : 0;
            opacity: 0.3;
        }

        .loader {
            --hue: 223;
            --bg: hsl(var(--hue), 10%, 90%);
            --fg: hsl(var(--hue), 10%, 10%);
            font-size: calc(16px + (24 - 16) * (100vw - 320px) / (1280 - 320));
        }

        .loader {
            background: var(--bg);
            color: var(--fg);
            font: 1em/1.5 sans-serif;
            height: 100vh;
            display: grid;
            place-items: center;
        }

        .loader .pl {
            display: block;
            width: 6.25em;
            height: 6.25em;
        }

        .pl__ring,
        .pl__ball {
            animation: ring 2s ease-out infinite;
        }

        .pl__ball {
            animation-name: ball;
        }

        /* Dark theme  */
        @media (prefers-color-scheme: dark) {
            :root {
                --bg: hsl(var(--hue), 10%, 10%);
                --fg: hsl(var(--hue), 10%, 90%);
            }
        }

        /* Animation */
        @keyframes ring {
            from {
                stroke-dasharray: 0 257 0 0 1 0 0 258;
            }

            25% {
                stroke-dasharray: 0 0 0 0 257 0 258 0;
            }

            50%,
            to {
                stroke-dasharray: 0 0 0 0 0 515 0 0;
            }
        }

        @keyframes ball {

            from,
            50% {
                animation-timing-function: ease-in;
                stroke-dashoffset: 1;
            }

            64% {
                animation-timing-function: ease-in;
                stroke-dashoffset: -109;
            }

            78% {
                animation-timing-function: ease-in;
                stroke-dashoffset: -145;
            }

            92% {
                animation-timing-function: ease-in;
                stroke-dashoffset: -157;
            }

            57%,
            71%,
            85%,
            99%,
            to {
                animation-timing-function: ease-out;
                stroke-dashoffset: -163;
            }
        }
    </style>
</head>

<body>

    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container">
            <a class="navbar-brand" href="/kecamatan">{{ config('app.name') }}</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent d-flex justify-content-between">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link @yield('calepLink', '')}" aria-current="page" href="/kecamatan">Calon
                            Pemilih</a>
                    </li>
                </ul>
                <a class="btn btn-dark" aria-current="page" href="/logout">Logout</a>
            </div>
        </div>
    </nav>
    <div class="container">
        @yield('slot')
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"></script>
    @yield('js')
</body>

</html>
