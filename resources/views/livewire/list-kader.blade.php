<div>
    <h3 class="mt-3">
        Daftar Kader Kelurahan {{ $kelurahan['nama'] }}
    </h3>

    <hr>

    <div class="d-flex justify-content-end mb-3">
        <div class="input-group" style="max-width: 260px;margin-right:15px;">
            <span class="input-group-text" id="basic-addon1">Pencarian</span>
            <input type="text" class="form-control" placeholder="Kata pencarian" aria-label="Kata pencarian"
                wire:model="search">
        </div>
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModalImport"
            wire:click="add()" data-backdrop="static" style="margin-right: 15px;">
            Import Data
        </button>
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal"
            wire:click="add()" data-backdrop="static">
            Tambah Data
        </button>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col" style="max-width:20px;">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="mt-1">
                @foreach ($listKader as $index => $data)
                    <tr>
                        <th scope="row">{{ $index + 1 }}</th>
                        <td>{{ $data['nama'] }}</td>
                        <td class="d-flex justify-content-end">
                            <a href="/kelurahan/{{$selected_kelurahan_id}}/kader/{{$data["id"]}}" class="btn btn-info" style="margin-right: 15px;">Info Pemilih</a>
                            <button type="button" class="btn btn-warning" style="margin-right:15px;"
                                data-bs-toggle="modal" data-bs-target="#exampleModal"
                                wire:click="edit({{ $index }})" data-backdrop="static">
                                Edit
                            </button>
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#exampleModalDelete" wire:click="delete({{ $index }})"
                                data-backdrop="static">
                                Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="exampleModalImport" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true" wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Import data Pemilih</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    @if ($errorMessage)
                        <div class="alert alert-danger" role="alert">
                            {{ $errorMessage }}
                        </div>
                    @endif
                    <div class="input-group mb-3">
                        <input type="file" class="form-control" id="inputGroupFile01" wire:model="excelFile">
                    </div>
                </div>
                <div class="modal-footer">
                    @if ($excelFile)
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                            wire:click="emptyForm()">Close</button>
                        <button type="button" class="btn btn-danger" wire:click="import()"
                            data-bs-dismiss="modal">Import</button>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
        wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">{{ $mode == 'add' ? 'Tambah' : 'Edit' }} data
                        Kader</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    @if ($errorMessage)
                        <div class="alert alert-danger" role="alert">
                            {{ $errorMessage }}
                        </div>
                    @endif
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="nama"
                                aria-describedby="basic-addon3 basic-addon4" wire:model="nama">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    @if (!($nama == ''))
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                            wire:click="emptyForm()">Close</button>
                        <button type="button" class="btn btn-danger" wire:click="save()"
                            data-bs-dismiss="modal">Simpan</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalDelete" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true" wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Hapus data Kader</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    Apakah anda yakin menghapus data kader {{ $selected_kader['nama'] ?? '' }} ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                        wire:click="emptyForm()">Close</button>
                    <button type="button" class="btn btn-danger" wire:click="confirmDelete()"
                        data-bs-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="loading" wire:loading wire:loading wire:target="save">
        <div class="loader">
            <svg class="pl" viewBox="0 0 200 200" width="200" height="200"
                xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <linearGradient id="pl-grad1" x1="1" y1="0.5" x2="0" y2="0.5">
                        <stop offset="0%" stop-color="hsl(313,90%,55%)" />
                        <stop offset="100%" stop-color="hsl(223,90%,55%)" />
                    </linearGradient>
                    <linearGradient id="pl-grad2" x1="0" y1="0" x2="0" y2="1">
                        <stop offset="0%" stop-color="hsl(313,90%,55%)" />
                        <stop offset="100%" stop-color="hsl(223,90%,55%)" />
                    </linearGradient>
                </defs>
                <circle class="pl__ring" cx="100" cy="100" r="82" fill="none"
                    stroke="url(#pl-grad1)" stroke-width="36" stroke-dasharray="0 257 1 257"
                    stroke-dashoffset="0.01" stroke-linecap="round" transform="rotate(-90,100,100)" />
                <line class="pl__ball" stroke="url(#pl-grad2)" x1="100" y1="18" x2="100.01"
                    y2="182" stroke-width="36" stroke-dasharray="1 165" stroke-linecap="round" />
            </svg>
        </div>
    </div>
</div>
