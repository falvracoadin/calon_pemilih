<div>
    <h3 class="mt-3">
        Daftar Pemilih Kelurahan {{ $kelurahan['nama'] }} Kader {{ $kader['nama'] }}
    </h3>

    <hr>

    <div class="d-flex justify-content-end mb-3">
        <div class="input-group" style="max-width: 260px;margin-right:15px;">
            <span class="input-group-text" id="basic-addon1">Pencarian</span>
            <input type="text" class="form-control" placeholder="Kata pencarian" aria-label="Kata pencarian"
                wire:model="search">
        </div>

        <div class="input-group" style="max-width: 260px;margin-right:15px;">
            <select class="form-select" id="department" wire:model="filterDataMasalah">
                @foreach ($listPilihanMasalah as $data)
                    <option value="{{ $data }}">
                        {{ $data }}
                    </option>
                @endforeach
            </select>
        </div>

        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModalImport"
            wire:click="add()" data-backdrop="static" style="margin-right: 15px;">
            Import Data
        </button>
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#exampleModal"
            wire:click="add()" data-backdrop="static" style="margin-right: 15px;">
            Tambah Data
        </button>
        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#exampleModalDelete"
            wire:click="delete(-2)" data-backdrop="static">
            Hapus semua data
        </button>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col" style="max-width:20px;">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Alamat</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="mt-1">
                @foreach ($listPemilih as $index => $data)

                    @if($filterDataMasalah == 'Data Ganda' && $data["num_nik"] > 1)
                        <?php $isTampil = true; ?>
                    @elseif($filterDataMasalah == 'NIK tidak lengkap' && strlen($data["nik"]) != 16)
                        <?php $isTampil = true; ?>
                    @elseif($filterDataMasalah == 'NIK/Nama tidak ada' && (!$data["nama"] || !$data['nik']))
                        <?php $isTampil = true; ?>
                    @elseif($filterDataMasalah == 'Semua')
                        <?php $isTampil = true; ?>
                    @endif

                    @if($isTampil)
                        <tr style="
                            @if(strlen($data["nik"]) != 16)
                                background-color:#feb6b6;
                            @elseif($data["num_nik"] > 1)
                                background-color:yellow;
                            @elseif(!$data["nama"] || !$data['nik'])
                                background-color:#698eb5;
                            @endif
                        ">
                            <th scope="row">{{ $counter++ }}</th>
                            <td>{{ $data['nama'] }}</td>
                            <td>{{ $data['nik'] }}</td>
                            <td>{{ $data['alamat'] }}</td>
                            <td class="d-flex justify-content-end">
                                <a type="button" class="btn btn-info" style="margin-right: 15px;"
                                    href="{{url('/kelurahan/'.$selected_kelurahan_id.'/kader/'.$kader['id'].'/'.$data['nik'])}}" data-backdrop="static">
                                    Detail
                                </a>
                                <button type="button" class="btn btn-warning" style="margin-right: 15px;"
                                    data-bs-toggle="modal" data-bs-target="#exampleModal"
                                    wire:click="edit({{ $index }})" data-backdrop="static">
                                    Edit
                                </button>
                                <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                    data-bs-target="#exampleModalDelete" wire:click="delete({{ $index }})"
                                    data-backdrop="static">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endif

                    <?php $isTampil = false; ?>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
        wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">{{ $mode == 'add' ? 'Tambah' : 'Edit' }} data
                        Pemilih</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    @if ($errorMessage)
                        <div class="alert alert-danger" role="alert">
                            {{ $errorMessage }}
                        </div>
                    @endif
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="nama"
                                aria-describedby="basic-addon3 basic-addon4" wire:model="nama">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="nik" class="form-label">NIK</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="nik"
                                aria-describedby="basic-addon3 basic-addon4" wire:model="nik">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="nik" class="form-label">Alamat</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="nik"
                                aria-describedby="basic-addon3 basic-addon4" wire:model="alamat">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                        wire:click="emptyForm()">Close</button>
                    <button type="button" class="btn btn-danger" wire:click="save()"
                        data-bs-dismiss="modal">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalDelete" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true" wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Hapus data pemilih</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    @if ($mode != 'delete-all')
                    Apakah anda yakin menghapus data pemilih {{ $selected_pemilih['nama'] ?? '' }}?
                    @else
                    Apakah anda yakin menghapus semua data pemilih pada kader {{$kader['nama']}} ?
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                        wire:click="emptyForm()">Close</button>
                    <button type="button" class="btn btn-danger" wire:click="confirmDelete()"
                        data-bs-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalDetail" tabindex="-1" aria-labelledby="exampleModalDetail"
        aria-hidden="true" wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title fs-5" id="exampleModalLabel">Informasi Data Ganda</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="closeDetailInfo()"></button>
                </div>
                <div class="modal-body">

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nama</th>
                                    <th scope="col">NIK</th>
                                    <th scope="col">Kader</th>
                                    <th scope="col">Kelurahan</th>
                                    <th scope="col">Kecamatan</th>
                                </tr>
                            </thead>
                            <tbody class="mt-1">
                                @foreach ($dataDouble  as $index => $data)
                                    <tr >
                                        <td>{{ $data['nama'] }}</td>
                                        <td>{{ $data['nik'] }}</td>
                                        <td>{{ $data['kader']['nama'] }}</td>
                                        <td>{{$data['kelurahan']['nama']}}</td>
                                        <td>{{$data['kelurahan']['kecamatan']['nama']}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalImport" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true" wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Import data Pemilih</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    @if ($errorMessage)
                        <div class="alert alert-danger" role="alert">
                            {{ $errorMessage }}
                        </div>
                    @endif
                    <div class="input-group mb-3">
                        <input type="file" class="form-control" id="inputGroupFile01" wire:model="excelFile">
                    </div>
                </div>
                <div class="modal-footer">
                    @if ($excelFile)
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                            wire:click="emptyForm()">Close</button>
                        <button type="button" class="btn btn-danger" wire:click="import()"
                            data-bs-dismiss="modal">Import</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade {{ $isCompare ? 'show' : '' }}" tabindex="-1" aria-labelledby="exampleModalLabel"
        @if ($isCompare) style="display: block; background-color: #48484890;" @endif
        aria-hidden="true" data-bs-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Resolve Data</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="clear()"></button>
                </div>
                <div class="modal-body">
                    <h5 class="mb-4">Terdapat {{ $failCount }} data yang bermasalah!</h5>
                    @for ($i = 0; $i < $failCount; $i++)
                        @if ($compare[$i]['kader_id'] == $failed[$i]['kader_id'])
                            <p style="font-weight:700;">NIK telah terdaftar pada kader {{ $kader['nama'] }}!</p>
                            <div class="row">
                                <div class="col-6 bg-warning text-dark bg-warning text-center">Data Lama</div>
                                <div class="col-6 bg-warning text-dark bg-warning text-center">Data Baru</div>
                                <div class="col-2">
                                    Nama
                                </div>
                                <div class="col-4">
                                    : {{ $compare[$i]['nama'] }}
                                </div>
                                <div class="col-2">
                                    Nama
                                </div>
                                <div class="col-4">
                                    : {{ $failed[$i]['nama'] }}
                                </div>
                                <div class="col-2">
                                    NIK
                                </div>
                                <div class="col-4">
                                    : {{ $compare[$i]['nik'] }}
                                </div>
                                <div class="col-2">
                                    NIK
                                </div>
                                <div class="col-4">
                                    : {{ $failed[$i]['nik'] }}
                                </div>
                                <div class="col-2">
                                    Alamat
                                </div>
                                <div class="col-4">
                                    : {{ $compare[$i]['alamat'] }}
                                </div>
                                <div class="col-2">
                                    Alamat
                                </div>
                                <div class="col-4">
                                    : {{ $failed[$i]['alamat'] }}
                                </div>
                            </div>

                            <p class="mt-3" style="font-weight: 700;">Pilih data baru atau data lama?</p>
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="flexRadioDefault{{ $i }}"
                                    id="flexRadioDefault1{{ $i }}"
                                    wire:model.defer="resolveData.{{ $i }}" value="0">
                                <label class="form-check-label" for="flexRadioDefault1{{ $i }}">
                                    Data Lama
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="flexRadioDefault{{ $i }}"
                                    id="flexRadioDefault2{{ $i }}"
                                    wire:model.defer="resolveData.{{ $i }}" value="1">
                                <label class="form-check-label" for="flexRadioDefault2{{ $i }}">
                                    Data Baru
                                </label>
                            </div>
                        @else
                            <p style="font-weight:700;">NIK telah terdaftar pada kader
                                {{ $compare[$i]['nama_kader'] }}!</p>
                            <div class="row">
                                <div class="col-6 bg-warning text-dark bg-warning text-center">Data Lama</div>
                                <div class="col-6 bg-warning text-dark bg-warning text-center">Data Baru</div>
                                <div class="col-2">
                                    Nama
                                </div>
                                <div class="col-4">
                                    : {{ $compare[$i]['nama'] }}
                                </div>
                                <div class="col-2">
                                    Nama
                                </div>
                                <div class="col-4">
                                    : {{ $failed[$i]['nama'] }}
                                </div>
                                <div class="col-2">
                                    NIK
                                </div>
                                <div class="col-4">
                                    : {{ $compare[$i]['nik'] }}
                                </div>
                                <div class="col-2">
                                    NIK
                                </div>
                                <div class="col-4">
                                    : {{ $failed[$i]['nik'] }}
                                </div>
                                <div class="col-2">
                                    Alamat
                                </div>
                                <div class="col-4">
                                    : {{ $compare[$i]['alamat'] }}
                                </div>
                                <div class="col-2">
                                    Alamat
                                </div>
                                <div class="col-4">
                                    : {{ $failed[$i]['alamat'] }}
                                </div>
                            </div>
                            <p class="mt-3" style="font-weight: 700;">Pilih Kader untuk pemilih?</p>
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="flexRadioDefault{{ $i }}"
                                    id="flexRadioDefault1{{ $i }}"
                                    wire:model.defer="resolveData.{{ $i }}" value="0">
                                <label class="form-check-label" for="flexRadioDefault1{{ $i }}">
                                    {{$compare[$i]['nama_kader']}}
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio"
                                    name="flexRadioDefault{{ $i }}"
                                    id="flexRadioDefault2{{ $i }}"
                                    wire:model.defer="resolveData.{{ $i }}" value="1">
                                <label class="form-check-label" for="flexRadioDefault2{{ $i }}">
                                    {{$kader["nama"]}}
                                </label>
                            </div>
                        @endif
                        <hr class="mt-5">
                    @endfor
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                        wire:click="clear()">Close</button>
                    <button type="button" class="btn btn-danger" wire:click="resolve()"
                        data-bs-dismiss="modal">Simpan</button>
                </div>
            </div>
        </div>
    </div>


    <div class="loading" wire:loading wire:loading wire:target="save">
        <div class="loader">
            <svg class="pl" viewBox="0 0 200 200" width="200" height="200"
                xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <linearGradient id="pl-grad1" x1="1" y1="0.5" x2="0" y2="0.5">
                        <stop offset="0%" stop-color="hsl(313,90%,55%)" />
                        <stop offset="100%" stop-color="hsl(223,90%,55%)" />
                    </linearGradient>
                    <linearGradient id="pl-grad2" x1="0" y1="0" x2="0" y2="1">
                        <stop offset="0%" stop-color="hsl(313,90%,55%)" />
                        <stop offset="100%" stop-color="hsl(223,90%,55%)" />
                    </linearGradient>
                </defs>
                <circle class="pl__ring" cx="100" cy="100" r="82" fill="none"
                    stroke="url(#pl-grad1)" stroke-width="36" stroke-dasharray="0 257 1 257"
                    stroke-dashoffset="0.01" stroke-linecap="round" transform="rotate(-90,100,100)" />
                <line class="pl__ball" stroke="url(#pl-grad2)" x1="100" y1="18" x2="100.01"
                    y2="182" stroke-width="36" stroke-dasharray="1 165" stroke-linecap="round" />
            </svg>
        </div>
    </div>
</div>
