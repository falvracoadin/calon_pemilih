<div>
    <h3 class="mt-3">
        Daftar Pemilih Kembar NIK {{$nikView}}
    </h3>

    <hr>

    <div class="d-flex justify-content-end mb-3">
        <div class="input-group" style="max-width: 260px;margin-right:15px;">
            <span class="input-group-text" id="basic-addon1">Pencarian</span>
            <input type="text" class="form-control" placeholder="Kata pencarian" aria-label="Kata pencarian"
                wire:model="search">
        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col" style="max-width:20px;">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Kader</th>
                    <th scope="col">Kelurahan</th>
                    <th scope="col">Kecamatan</th>
                    <th scope="col">Alamat</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody class="mt-1">
                @foreach ($listPemilih as $index => $data)
                    <tr>
                        <th scope="row">{{ $index + 1 }}</th>
                        <td>{{ $data['nama'] }}</td>
                        <td>{{ $data['nik'] }}</td>
                        <td>{{ $data['kader']['nama'] ?? '-' }}</td>
                        <td>{{ $data['kelurahan']['nama'] ?? '-' }}</td>
                        <td>{{ $data['kelurahan']['kecamatan']['nama'] ?? '-' }}</td>
                        <td>{{ $data['alamat'] }}</td>
                        <td class="d-flex justify-content-end">
                            <button type="button" class="btn btn-warning" style="margin-right: 15px;"
                                data-bs-toggle="modal" data-bs-target="#exampleModal"
                                wire:click="edit({{ $index }})" data-backdrop="static">
                                Edit
                            </button>
                            <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#exampleModalDelete" wire:click="delete({{ $index }})"
                                data-backdrop="static">
                                Delete
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"
        wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">{{ $mode == 'add' ? 'Tambah' : 'Edit' }} data
                        Pemilih</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    @if ($errorMessage)
                        <div class="alert alert-danger" role="alert">
                            {{ $errorMessage }}
                        </div>
                    @endif
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="nama"
                                aria-describedby="basic-addon3 basic-addon4" wire:model="nama">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="nik" class="form-label">NIK</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="nik"
                                aria-describedby="basic-addon3 basic-addon4" wire:model="nik">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="nik" class="form-label">Alamat</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="nik"
                                aria-describedby="basic-addon3 basic-addon4" wire:model="alamat">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                        wire:click="emptyForm()">Close</button>
                    <button type="button" class="btn btn-danger" wire:click="save()"
                        data-bs-dismiss="modal">Simpan</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalDelete" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true" wire:ignore.self data-bs-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="exampleModalLabel">Hapus data pemilih</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        wire:click="emptyForm()"></button>
                </div>
                <div class="modal-body">
                    Apakah anda yakin menghapus data pemilih {{ $selected_pemilih['nama'] ?? '' }}?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"
                        wire:click="emptyForm()">Close</button>
                    <button type="button" class="btn btn-danger" wire:click="confirmDelete()"
                        data-bs-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
