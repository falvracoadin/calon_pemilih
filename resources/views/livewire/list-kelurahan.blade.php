<div>
    <h3 class="mt-3">
        Daftar Kelurahan Pemilih Kecamatan {{ $kecamatan['nama'] }}
    </h3>

    <hr>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col" style="max-width:20px;">No</th>
                    <th scope="col">Kelurahan</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <div class="d-flex justify-content-end">
                <div class="input-group mb-3" style="max-width: 260px;">
                    <span class="input-group-text" id="basic-addon1">Pencarian</span>
                    <input type="text" class="form-control" placeholder="Kata pencarian" aria-label="Kata pencarian"
                        wire:model="search">
                </div>
            </div>
            <tbody class="mt-1">
                @foreach ($listKelurahan as $index => $data)
                    <tr>
                        <th scope="row">{{ $counter++ }}</th>
                        <td>{{ $data['nama'] }}</td>
                        <td class="d-flex justify-content-end">
                            <a class="btn btn-warning" style="margin-right: 20px"
                                href="{{ '/kelurahan/' . $data['id'] . '/kader' }}">Info</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-end mt-3">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" style="color:black; cursor:pointer;"
                        wire:click="toPage({{ $page - 1 }})">Previous</a></li>
                @for ($i = 1; $i <= $maxPage; $i++)
                    <li class="page-item">
                        <a class="page-link @if ($i == $page) bg-warning @endif" style="color:black;cursor: pointer;" wire:click="toPage({{$i}})">{{$i}}</a>
                    </li>
                @endfor()
                <li class="page-item"><a class="page-link" style="color:black;cursor: pointer;"
                        wire:click="toPage({{ $page + 1 }})">Next</a></li>
            </ul>
        </nav>
    </div>

    <div class="loading" wire:loading>
        <div class="loader">
            <svg class="pl" viewBox="0 0 200 200" width="200" height="200" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <linearGradient id="pl-grad1" x1="1" y1="0.5" x2="0" y2="0.5">
                        <stop offset="0%" stop-color="hsl(313,90%,55%)" />
                        <stop offset="100%" stop-color="hsl(223,90%,55%)" />
                    </linearGradient>
                    <linearGradient id="pl-grad2" x1="0" y1="0" x2="0" y2="1">
                        <stop offset="0%" stop-color="hsl(313,90%,55%)" />
                        <stop offset="100%" stop-color="hsl(223,90%,55%)" />
                    </linearGradient>
                </defs>
                <circle class="pl__ring" cx="100" cy="100" r="82" fill="none" stroke="url(#pl-grad1)" stroke-width="36" stroke-dasharray="0 257 1 257" stroke-dashoffset="0.01" stroke-linecap="round" transform="rotate(-90,100,100)" />
                <line class="pl__ball" stroke="url(#pl-grad2)" x1="100" y1="18" x2="100.01" y2="182" stroke-width="36" stroke-dasharray="1 165" stroke-linecap="round" />
            </svg>
        </div>
    </div>

</div>
