<div>
    <h3 class="mt-3">
        Daftar Kecamatan Pemilih
    </h3>

    <hr>

    <table class="table">
        <thead>
            <tr>
                <th scope="col" style="max-width:20px;">No</th>
                <th scope="col">Kecamatan</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <div class="d-flex justify-content-end">
            <div class="input-group mb-3" style="max-width: 260px;">
                <span class="input-group-text" id="basic-addon1">Pencarian</span>
                <input type="text" class="form-control" placeholder="Kata pencarian" aria-label="Kata pencarian"
                    wire:model="search">
            </div>
        </div>
        <tbody class="mt-1">
            @foreach ($listKecamatan as $index => $data)
                <tr>
                    <th scope="row">{{ $counter++ }}</th>
                    <td>{{ $data['nama'] }}</td>
                    <td class="d-flex justify-content-end">
                        <a class="btn btn-warning" style="margin-right: 20px"
                            wire:click="toKelurahan({{$data}})">Info</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>
