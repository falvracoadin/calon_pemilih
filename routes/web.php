<?php

use App\Http\Livewire\ListKader;
use App\Http\Livewire\ListKecamatan;
use App\Http\Livewire\ListKelurahan;
use App\Http\Livewire\ListPemilih;
use App\Http\Livewire\ListPemilihKembar;
use App\Http\Livewire\Login;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function(){
    Route::get('/kelurahan/{id}', ListKelurahan::class);

    Route::get('/kelurahan/{id}/kader', ListKader::class);

    Route::get('/kelurahan/{id}/kader/{kaderId}', ListPemilih::class);

    Route::get('/kelurahan/{id}/kader/{kaderId}/{nikView}', ListPemilihKembar::class);

    Route::get('/kecamatan', ListKecamatan::class);

    Route::get('/', ListKecamatan::class);

    Route::get('/logout', function(){
        session()->invalidate();
        return redirect()->intended('/login');
    });
});

Route::middleware(["guest"])->group(function(){
    Route::get('/login', Login::class)->name('login');
});
