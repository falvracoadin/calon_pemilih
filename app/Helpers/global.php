<?php

use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;

function parseExcel($filePath)
{
    $reader = IOFactory::createReader('Xlsx');
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load(Storage::path($filePath));
    $sheet = $spreadsheet->getActiveSheet();

    $highestRow = $sheet->getHighestRow();
    $highestColumn = $sheet->getHighestColumn();
    $highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);
    $result = [];
    for ($row = 3; $row <= $highestRow; $row++) {
        $data = [];
        for ($col = 1; $col <= $highestColumnIndex; $col++) {
            $value = $sheet->getCellByColumnAndRow($col, $row)->getValue();
            array_push($data, $value);
        }
        array_push($result, $data);
    }
    return $result;
}

function parseExcelAllSheet($filePath)
{
    $reader = IOFactory::createReader("Xlsx");
    $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load(Storage::path($filePath));
    foreach ($spreadsheet->getSheetNames() as $sheetName) {
        $sheet = $spreadsheet->getSheetByName($sheetName);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);
        $result[$sheetName] = [];
        for ($row = 3; $row <= $highestRow; $row++) {
            $data = [];
            for ($col = 1; $col <= $highestColumnIndex; $col++) {
                $value = $sheet->getCell([$col, $row])->getValue();
                array_push($data, $value);
            }
            array_push($result[$sheetName], $data);
        }
    }
    return $result;
}
