<?php

namespace App\Http\Livewire;

use App\Models\Kader;
use App\Models\KecamatanModel;
use App\Models\KelurahanModel;
use App\Models\Pemilih;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use PDO;

class ListPemilih extends Component
{
    use WithFileUploads;

    public $nama, $nik, $m_kelurahan_id, $alamat; //for inpu
    public $m_kecamatan_id; //for get kelurahan
    public $listPemilih = [];
    public $isOpenModal;
    public $listKecamatan, $listKelurahan = [];
    public $mode = 'add';
    public $counter = 0;
    public $page = 1;
    public $limit = 10;
    public $search = '';
    public $maxPage = 0;
    public $selected_kelurahan_id, $selected_pemilih_id,$selected_pemilih;
    public $kelurahan;
    public $errorMessage = '';
    public $kader;
    public $excelFile;
    public $kecamatan;

    //compare view
    public $isCompare = false;
    public $failed = [];
    public $compare = [];
    public $failCount = 0;
    public $resolveData = [];
    public $dataDouble = [], $isDetail;
    public $filterDataMasalah, $listPilihanMasalah, $isTampil = false;
    public function mount($id, $kaderId)
    {
        Artisan::call('cache:clear');
        $this->selected_kelurahan_id = $id;
        $this->kelurahan = KelurahanModel::find($id);
        $this->kader = Kader::find($kaderId);
        if(!$this->kader){
            abort(404);
        }
        $this->kader = $this->kader->toArray();
        $this->updatePemilih();

        $this->listPilihanMasalah = ['Semua','Data Ganda', 'NIK tidak lengkap','NIK/Nama tidak ada'];
        $this->filterDataMasalah = 'Semua';
    }

    public function import(){
        if($this->excelFile){
            $status = $this->validate([
                'excelFile' => 'mimetypes:application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]);
            $now = now();
            $newFilename = $this->kelurahan["id"] . '_' . $this->kader["id"] . "_" . $now->timestamp . '_.' . $this->getExtension($this->excelFile);
            $status = $this->excelFile->storeAs($this->kelurahan["nama"], $newFilename);
            if($status){
                $this->excelFile = null;
            }
            $data = Pemilih::insertDataExcel($status, $this->kelurahan["id"], $this->kader["id"]);
            if($data["failCount"] > 0){
                $this->compare = $data["compare"];
                $this->failed = $data["failed"];
                $this->isCompare = true;
                $this->failCount = $data["failCount"];
                $this->resolveData = array_fill(0, $this->failCount, 0);
            }
            $this->updatePemilih();
        }
    }

    public function getInfoDetail($index)
    {
        $val = $this->listPemilih[$index];
        $cek = Pemilih::with(['kader', 'kelurahan.kecamatan'])->where('nik', $val['nik'])->where('id','!=', $val['id'])->get();
        $temp = [];
        foreach($cek as $val){
            if($val->kader != null){
                $temp[] = $val;
            }
        }
        $this->dataDouble = $temp;
        $this->isDetail = true;
    }

    public function closeDetailInfo()
    {
        $this->isDetail = false;
    }

    public function clear(){
        $this->compare = [];
        $this->failed = [];
        $this->isCompare = false;
        $this->failCount = 0;
        $this->resolveData = [];
    }

    public function getExtension($file){
        $fileName = $file->getFileName();
        $explode = explode('.', $fileName);
        return $explode[sizeof($explode) - 1];
    }

    public function updatePemilih()
    {
        $kaderId = $this->kader['id'];
        $this->listPemilih = DB::select("select pemilih.*, nums.num_nik from pemilih
            join (select nik, count(*) as num_nik from pemilih group by nik) as nums on nums.nik = pemilih.nik
            where pemilih.m_kelurahan_id = '$this->selected_kelurahan_id' and
                pemilih.kader_id = '$kaderId' and
                (pemilih.nama like '%$this->search%' or pemilih.nik like '%$this->search%')
            order by id asc");
        $this->listPemilih = json_decode(json_encode($this->listPemilih), true);
        // dd($this->listPemilih);
        $isTampil = 1;
        foreach ($this->listPemilih as &$pemilih) {
            $isTampil = 1;

            if (isset($pemilih['num_nik']) && $pemilih['num_nik'] > 1) {
                $cek = Pemilih::where('nik', $pemilih['nik'])->pluck('kader_id');

                foreach ($cek as $val) {
                    $cek2 = Kader::where('id', $val)->first();
                    if (empty($cek2)) {

                        $isTampil = 0;
                        break;
                    }
                }
            }
            if ($isTampil == 0) {
                $pemilih['num_nik'] = 1;
            }
        }
        $this->maxPage = ceil(sizeof($this->listPemilih) / $this->limit);
    }

    public function resolve(){
        for($i = 0; $i < $this->failCount; $i++){
            if($this->resolveData[$i] == "0"){continue;}
            $old = $this->compare[$i];
            $new = $this->failed[$i];
            if($old["kader_id"] == $new["kader_id"]){
                Pemilih::where('id', $old['id'])
                    ->update($new);
            }else{
                Pemilih::where('id', $old['id'])
                    ->update([
                        'kader_id' => $this->kader['id'],
                        'm_kelurahan_id' => $this->kader['m_kelurahan_id']
                    ]);
            }
        }
        $this->clear();
        $this->updatePemilih();
    }

    public function openModal()
    {
        $this->isOpenModal = true;
    }

    public function closeModal()
    {
        $this->isOpenModal = false;
    }

    public function toPage($page){
        if($page < 1 || $page > $this->maxPage)return;
        $this->page = $page;
    }

    public function isNotValid(){
        return $this->nama == '' ||
            $this->nik == '' ||
            $this->m_kecamatan_id == '' ||
            $this->m_kelurahan_id == '';
    }

    public function updated($name, $value)
    {
        if($name == 'search'){
            $this->updatePemilih();
        }
    }

    public function emptyForm()
    {
        $this->errorMessage = '';
        $this->nama = '';
        $this->nik = '';
        $this->alamat = '';
        $this->m_kecamatan_id = '';
        $this->m_kelurahan_id = '';
    }

    public function edit($index)
    {
        $val = $this->listPemilih[$index];
        $this->selected_pemilih_id = $val['id'];

        $this->nama = $val['nama'];
        $this->nik = $val['nik'];
        $this->alamat = $val['alamat'];
        $this->mode = 'edit';
        $this->openModal();
    }

    public function add()
    {
        $this->mode  = 'add';
        $this->openModal();
    }

    public function onClickKecamatan()
    {
        $this->listKelurahan = KelurahanModel::where('m_kecamatan_id', $this->m_kecamatan_id)->get();
    }

    public function delete($id)
    {
        if ($id == -2){
            $this->mode = 'delete-all';
            return;
        }
        $this->selected_pemilih_id = $this->listPemilih[$id]["id"];
        $this->selected_pemilih = $this->listPemilih[$id];
    }

    public function confirmDelete(){
        if($this->mode == 'delete-all'){
            Pemilih::where('kader_id', $this->kader["id"])
                ->delete();
        }else{
            Pemilih::where('id', $this->selected_pemilih_id)->delete();
        }
        $this->selected_pemilih_id = null;
        $this->selected_pemilih = [];
        $this->updatePemilih();
    }

    public function save()
    {
        if ($this->mode == 'add') {
            Pemilih::create([
                'nama' => $this->nama,
                'nik' => $this->nik,
                'alamat' => $this->alamat,
                'm_kelurahan_id' => $this->kelurahan["id"],
                'kader_id' => $this->kader["id"]
            ]);
            $this->afterSave();
        } else {
            Pemilih::where('id', $this->selected_pemilih_id)->update([
                'nama' => $this->nama,
                'nik' => $this->nik,
                'alamat' => $this->alamat,
                'm_kelurahan_id' => $this->kelurahan["id"],
                'kader_id' => $this->kader["id"]
            ]);
            $this->afterSave();
        }

        //alert

        $this->afterSave();
    }

    public function afterSave()
    {
        $this->closeModal();
        $this->emptyForm();
        $this->updatePemilih();
    }

    public function render()
    {
        $this->counter = 1;
        return view('livewire.list-pemilih')
            ->extends('app')
            ->section('slot');
    }
}
