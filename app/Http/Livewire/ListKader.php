<?php

namespace App\Http\Livewire;

use App\Models\Kader;
use App\Models\KecamatanModel;
use App\Models\KelurahanModel;
use Livewire\Component;
use Livewire\WithFileUploads;

class ListKader extends Component
{
    use WithFileUploads;
    public $nama, $m_kelurahan_id;
    public $m_kecamatan_id;
    public $listKader = [];
    public $listKecamatan= [], $listKelurahan = [];
    public $mode = 'add';
    public $page = 1;
    public $limit = 10;
    public $search = '';
    public $maxPage = 0;
    public $kelurahan, $kecamatan;
    public $errorMessage = '';
    public $selected_kelurahan_id, $selected_kader_id, $selected_kader;

    public $excelFile;

    public function mount($id){
        $this->selected_kelurahan_id = $id;
        $this->kelurahan = KelurahanModel::find($id);
        if(!$this->kelurahan){
            abort(404);
        }
        $this->updateViewData();
    }

    public function import(){
        if($this->excelFile){
            $status = $this->validate([
                'excelFile' => 'mimetypes:application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            ]);
            $now = now();
            $fileName = $this->kelurahan["id"] . '_' . $now->timestamp . '_' . $this->getExtension($this->excelFile);
            $status = $this->excelFile->storeAs($this->kelurahan["nama"], $fileName);
            if($status){
                $this->excelFile = null;
            }
            // dd(memory_get_usage());
            $statusImport = Kader::importDataExcel($status, $this->kelurahan["id"]);
            $this->updateViewData();
        }
    }

    public function getExtension($file){
        $fileName = $file->getFileName();
        $explode = explode('.', $fileName);
        return $explode[sizeof($explode) - 1];
    }

    public function updateViewData(){
        $query = Kader::where('m_kelurahan_id', $this->selected_kelurahan_id)
            ->where('nama', 'like', "%$this->search%");

        $this->maxPage = ceil($query->count() / $this->limit);
        $this->listKader = $query->get();
    }

    public function toPage($page){
        if($page < 1 || $page > $this->maxPage)return;
        $this->page = $page;
    }

    public function updated($name, $value){
        if($name == "search"){
            $this->updateViewData();
        }
    }

    public function emptyForm(){
        $this->errorMessage = '';
        $this->nama = '';
        $this->m_kecamatan_id = '';
        $this->m_kelurahan_id = '';
    }

    public function edit($index){
        $val = $this->listKader[$index];
        $this->selected_kader_id = $val->id;

        $this->nama = $val->nama;
        $this->mode = 'edit';
    }

    public function add(){
        $this->mode = 'add';
    }

    public function delete($id){
        $this->selected_kader = $this->listKader[$id];
    }

    public function confirmDelete(){
        Kader::where('id', $this->selected_kader["id"])->delete();
        $this->selected_kader = [];
        $this->updateViewData();
    }

    public function save(){
        if ($this->mode == 'add'){
            Kader::create([
                "nama" => $this->nama,
                "m_kelurahan_id" => $this->kelurahan["id"]
            ]);
        }else if($this->mode == "edit"){
            Kader::where('id', $this->selected_kader_id)->update([
                "nama" => $this->nama,
                "m_kelurahan_id" => $this->kelurahan["id"]
            ]);
        }
        $this->updateViewData();
        $this->emptyForm();
    }
    public function render()
    {
        return view('livewire.list-kader')
            ->extends('app')
            ->section('slot');
    }
}
