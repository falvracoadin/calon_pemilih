<?php

namespace App\Http\Livewire;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Login extends Component
{
    public $username, $password;
    public $errorMessage;

    public function mount(){

    }

    public function login(){
        $credentials = [
            'name' => $this->username,
            "password" => $this->password
        ];
        if(Auth::attempt($credentials)){
            session()->regenerate();
            return redirect()->intended('/kecamatan');
        }else{
            $this->errorMessage = 'kredensial untuk data pengguna tidak terdaftar!';
            $this->password = '';
        }
    }

    public function render()
    {
        return view('livewire.login')
            ->extends("admin")
            ->section('slot');
    }
}
