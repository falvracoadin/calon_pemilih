<?php

namespace App\Http\Livewire;

use App\Models\KecamatanModel;
use Livewire\Component;

class ListKecamatan extends Component
{
    public $listKecamatan = [];
    public $counter = 1;

    public $search = '';
    public $selected = null;
    public function mount()
    {
        $this->listKecamatan = KecamatanModel::get();

        $this->counter = 1;
    }

    public function toKelurahan($data){
        redirect('/kelurahan/'. $data["id"], 200);
    }

    public function updated(){
        $this->updateKecamatan();
    }

    public function updateKecamatan(){
        $this->listKecamatan = KecamatanModel::where('nama', 'like', "%$this->search%")
            ->get();
    }

    public function delete($index){
        $this->selected = $this->listKecamatan[$index];
    }

    public function confirmDelete(){
        $deleted = KecamatanModel::where("id" , '=', $this->selected["id"])
            ->delete();
        if($deleted){
            $this->updateKecamatan();
        }
    }


    public function render()
    {
        $this->counter = 1;
        return view('livewire.list-kecamatan')
            ->extends("app")
            ->section('slot');
    }
}
