<?php

namespace App\Http\Livewire;

use App\Models\Kader;
use App\Models\KelurahanModel;
use App\Models\Pemilih;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder;
use Livewire\Component;

class ListPemilihKembar extends Component
{
    public  $limit = 10,
            $search = '';

    //for query data
    public $kelurahanId, $kaderId, $nikView;
    public $listPemilih = [], $pemilih = [];

    //for editing
    public $nama, $nik, $alamat, $selected_pemilih_id, $selected_pemilih; 
    public $isOpenModal = false, $mode = '';
    public $kelurahan;
    public $kader;
    public $m_kecamatan_id, $m_kelurahan_id;

    //alert
    public $errorMessage;

        
    public function mount($id, $kaderId, $nikView){
        $this->kelurahanId = $id;
        $this->kaderId = $kaderId;
        $this->nikView = $nikView;
        $this->kader = Kader::find($kaderId);
        $this->kelurahan = KelurahanModel::find($id);

        $this->updatePemilih();
    }

    public function updated($nama, $value)
    {
        if($nama == 'search'){
            $this->updatePemilih();
        }
    }

    public function edit($index){
        $val = $this->listPemilih[$index];
        $this->selected_pemilih_id = $val["id"];

        $this->nama = $val["nama"];
        $this->nik = $val["nik"];
        $this->alamat = $val["alamat"];
        $this->mode = 'edit';
        $this->openModal();
    }

    public function delete($id){
        if($id < 0){
            return;
        }
        $this->selected_pemilih_id = $this->listPemilih[$id]["id"];
        $this->selected_pemilih = $this->listPemilih[$id];
    }

    public function confirmDelete(){
        Pemilih::where('id', $this->selected_pemilih_id)->delete();

        $this->selected_pemilih_id = null;
        $this->selected_pemilih = [];
        $this->updatePemilih();
    }

    public function openModal()
    {
        $this->isOpenModal = true;
    }

    public function closeModal(){
        $this->isOpenModal = false;
    }

    public function save(){
        Pemilih::where('id', $this->selected_pemilih_id)->update([
            'nama' => $this->nama,
            'nik' => $this->nik,
            'alamat' => $this->alamat,
            'm_kelurahan_id' => $this->kelurahan["id"],
            'kader_id' => $this->kader["id"]
        ]);
        $this->afterSave();
    }

    public function emptyForm()
    {
        $this->errorMessage = '';
        $this->nama = '';
        $this->nik = '';
        $this->alamat = '';
        $this->m_kecamatan_id = '';
        $this->m_kelurahan_id = '';
    }

    public function afterSave()
    {
        $this->closeModal();
        $this->emptyForm();
        $this->updatePemilih();
    }

    public function updatePemilih(){
        $this->listPemilih = Pemilih::with(["kader", "kelurahan.kecamatan"])
            ->where("nik", $this->nikView)
            ->where(function(EloquentBuilder $query){
                $query->orWhere("pemilih.nama", 'like', "%$this->search%")
                    ->orWhere("pemilih.nik", 'like', "%$this->search%");
            })
            ->get()->toArray();
    }


    public function render()
    {
        return view('livewire.list-pemilih-kembar')
            ->extends('app')
            ->section('slot');
        }
}
