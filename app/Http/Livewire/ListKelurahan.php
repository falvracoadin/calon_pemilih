<?php

namespace App\Http\Livewire;

use App\Models\KecamatanModel;
use App\Models\KelurahanModel;
use Livewire\Component;

class ListKelurahan extends Component
{
    public $listKelurahan = [];
    public $kecamatan = null;
    public $selected = null;
    public $search = "";
    public $counter = 0;
    public $limit = 10;
    public $page = 1;
    public $maxPage = 0;
    public function mount($id)
    {
        $this->search = "";
        $this->kecamatan = KecamatanModel::find($id);
        if(!$this->kecamatan){
            abort(404);
        }
        $this->updateKelurahan();
    }


    public function delete($index)
    {
        $this->selected = $this->listKelurahan[$index];
    }

    public function confirmDelete()
    {
        $deleted = KelurahanModel::where("id", '=', $this->selected["id"])
            ->delete();
        if ($deleted) {
            $this->updateKelurahan();
        }
    }

    public function toPage($page = 0){
        if($page < 1 || $page > $this->maxPage) return;
        $this->page = $page;
        $this->updateKelurahan();
    }

    public function updateKelurahan()
    {
        $query = KelurahanModel::where('m_kecamatan_id', $this->kecamatan["id"    ])
        ->where('nama', 'like', "%$this->search%");

        $this->maxPage = ceil($query->count() / $this->limit);
        $this->listKelurahan = $query->skip(($this->page - 1) * $this->limit)
        ->limit($this->limit)
        ->get();
    }

    public function updated(){
        $this->updateKelurahan();
    }
    public function render()
    {
        $this->counter = 1;
        return view('livewire.list-kelurahan')
            ->extends("app")
            ->section("slot");
    }
}
