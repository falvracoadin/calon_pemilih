<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pemilih extends Model
{
    use HasFactory;
    protected $table = 'pemilih';
    public $timestamps = false;

    protected $fillable = [
        'nik',
        'nama',
        'alamat',
        'm_kelurahan_id',
        'kader_id'
    ];

    public function kader():BelongsTo
    {
        return $this->belongsTo(Kader::class, "kader_id");
    }

    public function kelurahan():BelongsTo
    {
        return $this->belongsTo(KelurahanModel::class, "m_kelurahan_id");
    }

    public static function insertDataExcel($pathFile, $m_kelurahan_id, $kaderId){
        $data = parseExcel($pathFile);
        $failed = [];
        $compare = [];
        $succesCount = 0;
        $failCount = 0;
        foreach($data as $val){
            if(empty($val[1]) || $val[1] == "" || $val[1] == null){
                continue;
            }
            if (empty($val[2]) || $val[2] == "" || $val[2] == null) {
                continue;
            }
            if(strtolower($val[1]) == 'nama' || strtolower($val[2]) == 'nik' || strtolower($val[3]) == 'alamat'){
                continue;
            }
            $pemilih = [
                'nama' => $val[1],
                'nik' => $val[2],
                'alamat' => $val[3],
                'm_kelurahan_id' => $m_kelurahan_id,
                'kader_id' => $kaderId
            ];

            //validasi ng kene yo

            try{
                self::create($pemilih);
            }catch(\Throwable $e){
                array_push($failed, $pemilih);
                $pemilihCompare = self::select('pemilih.*', 'kader.nama as nama_kader')
                    ->where('nik', $pemilih['nik'])
                    ->join('kader', 'kader.id', '=', 'pemilih.kader_id')
                    ->first();

                if(!empty($pemilihCompare)){
                    $pemilihCompare = $pemilihCompare->toArray();
                    array_push($compare, $pemilihCompare);
                }else{
                    continue;
                }

                $failCount ++;
                continue;
            }
            $succesCount ++;
        }
        return [
            'failed' => $failed,
            'compare' => $compare,
            'successCount' => $succesCount,
            'failCount' => $failCount
        ];
    }


}
