<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kader extends Model
{
    use HasFactory;
    protected $table = 'kader';
    public $timestamps = false;

    protected $fillable = [
        'nama',
        'm_kelurahan_id'
    ];

    public static function importDataExcel($pathFile, $m_kelurahan_id)
    {
        $data = parseExcelAllSheet($pathFile);
        foreach ($data as $kaderName => $val) {
            $kader = [
                'nama' => $kaderName,
                'm_kelurahan_id' => $m_kelurahan_id
            ];
            $kader = self::create($kader);
            if(!$kader){return false;}
            foreach ($val as $dataPemilih) {
                if(
                    ($dataPemilih[1] == '' || $dataPemilih[2] == '') ||
                     (strtolower($dataPemilih[1]) == 'nama' || strtolower($dataPemilih[2]) == 'nik' || strtolower($dataPemilih[3]) == 'alamat')
                    )
                Pemilih::create([
                    'nama' => $dataPemilih[1],
                    'nik' => $dataPemilih[2] ,
                    'alamat' => $dataPemilih[3] ?? '',
                    'm_kelurahan_id' => $m_kelurahan_id,
                    'kader_id' => $kader->id
                ]);
            }
        }
        return true;
    }
}
