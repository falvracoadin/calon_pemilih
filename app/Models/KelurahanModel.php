<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class KelurahanModel extends Model
{
    use HasFactory;
    protected $table = 'w_kelurahan';

    protected $keyType      = "string";

    protected $primaryKey   = "id";

    public function kecamatan():BelongsTo
    {
        return $this->belongsTo(KecamatanModel::class, "m_kecamatan_id");
    }
}
